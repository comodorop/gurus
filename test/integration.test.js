
const app = require('../server') // Link to your server file
const supertest = require('supertest')
const request = supertest(app)
const { validateMutants } = require("../services/mutants.services")
let dna = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
// jest.useFakeTimers();
// jest.setTimeout(600000);


describe('Endpoint Users', () => {
    it('Post ADN SUCCESS', async () => {
        let response = await request.post('/v1/mutations/').
            send({
                "dna": ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
            })
        expect(response.status === true)
    }, 70000);
    it('Post ADN WRONG', async () => {
        let response = await request.post('/v1/mutations/').
            send({
                "dna": ["ASDEWQ", "RTYHGF", "POIKLJ", "XCVBNM"]
            })
        expect(response.status === false)
    });
    it('Post ADN WRONG TYPE DATA', async () => {
        let response = await request.post('/v1/mutations/').
            send({
                "dna": "ATGCGA"
            })
        expect(response.status).toBe(400)
    });
    it('Post ADN EMPTY DATA', async () => {
        let response = await request.post('/v1/mutations/').
            send({
                "dna": []
            })
        expect(response.status).toBe(400)
    });
    it('Post ADN ONE ITEM', async () => {
        let response = await request.post('/v1/mutations/').
            send({
                "dna": ["ATGCGA"]
            })
        console.log(response.body)
        expect(response.status).toBe(400)
    });

});

describe('Test function mutans', () => {
    it('SUCCESS ADN', async () => {
        await validateMutants(dna).then(ok => {
            expect(ok.isMutant === true)
        }).catch(err => {
            console.log(err)
        })
    });
    it('WRONG ADN', async () => {
        dna = ["ASDEWQ", "RTYHGF", "POIKLJ", "XCVBNM"]
        await validateMutants(dna).then(ok => {
            expect(ok.isMutant === false)
        }).catch(err => {
            console.log(err)
        })
    });
});


