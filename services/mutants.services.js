const { extractDiagonalSup, extractDiagonalInf, exactColumn, exractRow } = require('../utils/matriz.js');
const { v4 } = require('uuid');
const { create } = require("../repository/mutation")

async function validateMutants(matriz) {
    let ok = verifiyMutants(matriz)
    let obj = {
        id: v4(),
        mutation: ok ? 1 : 0,
        nomutation: !ok ? 1 : 0
    }
    await create(obj) 
    return ok
}

function verifiyMutants(matriz) {
    let diags = extractDiags(matriz);
    let cols = extractCols(matriz);
    let rows = extractRows(matriz);
    let data = [...diags, ...cols, ...rows].find((line) => {
        let info = verifyConsecutive(line, 4)
        if (info)
            return info
    })
    if (data !== undefined && data.length > 0) {
        return true
    }
    return false
}

const extractDiags = (matriz) => {
    let tiras = [];
    matriz.forEach((col, i) => {
        tiras.push(extractDiagonalSup(matriz, i));
    })
    matriz.forEach((row, i) => {
        if (i > 0)
            tiras.push(extractDiagonalInf(matriz, i));
    });
    return tiras;
}

const extractCols = (matriz) => {
    return matriz.map((col, i) => exactColumn(matriz, i));
}

const extractRows = (matriz) => {
    return matriz.map((col, i) => exractRow(matriz, i));
}

const verifyConsecutive = (lineArray, tope) => {
    let last = '';
    let actual = '';
    let count = 0;
    for (let i = 0; i <= lineArray.length - 1; i++) {
        last = (i == 0) ? lineArray[0] : lineArray[i - 1];
        actual = lineArray[i];
        if (last == actual) {
            count++;
            if (count == tope)
                return true;
        }
    }
    return false;
}

module.exports = {
    validateMutants
}