const { getCountMutation, gteCountNoMutation } = require("../repository/mutation")

async function getStats() {
    let count_mutations = await getCountMutation()
    let count_no_mutations = await gteCountNoMutation()
    return {
        count_mutations: count_mutations[0].count,
        count_no_mutations: count_no_mutations[0].count
    }
}

module.exports = {
    getStats
}