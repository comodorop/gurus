const express = require("express")
const routes = express.Router()
const {getStats} = require("../services/stadisticas")

routes.get("/", async(req, res)=>{
    try {
        let data = await getStats()
        res.status(200).send({"msg": "statistics", data})
    } catch (error) {
        res.status(500).send("There is a problem in the server")
    }
})

module.exports = routes