const express = require("express")
const routes = express.Router()

const { validateMutants } = require("../services/mutants.services")

routes.post("/", async (req, res) => {
    let { dna } = req.body
    let isMutant = await validateMutants(dna)
    res.status(200).send({ isMutant })
})

module.exports = routes