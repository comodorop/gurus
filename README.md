
# Step to install project:

1.- Install dependecies execute the next command:
```
npm install
```

2.- Copy the the content .env.example and create a file .env with the content copy or execute next command:
```
cp .env.example .env
```
3.- After copy the content, change wit the particular values in every enviroments 

4.- Run de project execute the next command:
```
npm run start
```

## Test the project
If you want to run the test, execute next coomand:

```
npm run test
```
## Technical aspects
- node V16.1.0
- Postgres


Link to check documentation:
http://localhost:3000/api-docs/


## Description server:
Server: http://155.138.175.248:3232

If you wanto to check the documentation in web go next link
http://155.138.175.248:3232/api-docs/

