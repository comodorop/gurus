require('dotenv').config()
const express = require("express")
const app = express()
const bodyParser = require('body-parser')
const cors = require("cors")
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
const mutation = require("./routes/mutation.routes")
const statistics = require("./routes/stats.routes")
const {validateMutations} = require("./middleware/validation")
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use("/v1/mutations", validateMutations, mutation)
app.use("/v1/stats", statistics)


module.exports = app