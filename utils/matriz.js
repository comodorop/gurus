
function convertMatriz(array) {
  let finalMtx = [];
  array.forEach((ele) => {
    let tmpArray = [];
    for (let i = 0; i <= ele.length - 1; i++)
      tmpArray.push(ele[i]);
    finalMtx.push(tmpArray);
  });
  return finalMtx;
}

function exactColumn(matriz, nCol) {
  let groups = [];
  for (let i = 0; i <= matriz.length - 1; i++)
    groups.push(matriz[i][nCol]);
  return groups;
}

function exractRow(matriz, row) {
  let groups = [];
  for (let i = 0; i <= matriz.length - 1; i++)
    groups.push(matriz[row][i]);
  return groups;
}

function extractDiagonalSup(matriz, col) {
  let line = [];
  for (let i = 0; i <= matriz.length - col - 1; i++) {
    line.push(matriz[i][i + col]);
  }
  return line;
}

function extractDiagonalInf(matriz, row) {
  let line = [];
  for (let i = 0; i <= matriz.length - row - 1; i++)
    line.push(matriz[i + row][i]);
  return line;
}

module.exports = {
  convertMatriz,
  exactColumn,
  exractRow,
  extractDiagonalSup,
  extractDiagonalInf
}
