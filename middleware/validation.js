async function validateMutations(req, res, next) {
    let { body } = req
    if (body.hasOwnProperty("dna")) {
        if (!Array.isArray(body.dna)) {
            return res.status(400).send({ status: 400, msg: "Its require type array dna" })
        } else {
            if (body.dna.length <= 1) {
                return res.status(400).send({ status: 400, msg: "Its require two or more items of dna" })
            } else {
                next()
            }
        }
    } else {
        return res.status(400).send({ status: 400, msg: "Its require property dna" })
    }
}

module.exports = {
    validateMutations
}