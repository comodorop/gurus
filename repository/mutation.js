const {connection } = require('../connection/index')

async function create(body) {
    let knex = await connection()
    return await knex('logsmutations').insert(body)
}

async function getCountMutation(body) {
    let knex = await connection()
    return await knex('logsmutations').count().where({mutation: 1})
}

async function gteCountNoMutation(body) {
    let knex = await connection()
    return await knex('logsmutations').count().where({nomutation: 1})
}

module.exports = {
    create,
    getCountMutation,
    gteCountNoMutation
}