require('dotenv').config()
const knex = require('knex')
let cn;

async function connection() {
     return require('knex')({
        client: "pg",
        connection: {
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            user: process.env.DB_USER,
            database: process.env.DB_DATABASE,
            password: process.env.DB_PASSWORD
        },
        pool: { min: 2, max: 20 }
    })
}

async function getConnection() {
    return cn
}

module.exports = {
    connection,
    getConnection
}

